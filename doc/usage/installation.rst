Installation
============

.. tabs::

   .. tab:: Pip

      .. code-block:: console

         $ pip install rapidpe-rift-pipe

   .. tab:: Conda

      TODO

   .. tab:: Source

      .. code-block:: console

         $ git clone git@git.ligo.org:rapidpe-rift/rapidpe-rift-pipe.git
         $ cd rapidpe-rift-pipe
         $ pip install .
