=============
Configuration
=============

-------
General
-------

.. confval:: output_parent_directory

   :type: string

   The parent directory in which to create the output directory.


.. confval:: submit_only_at_exact_signal_position

   :type: integer
   :default: ``0``

   If set to ``1``, submits a single ILE job at the event parameters, and
   performs no AMR.  If set to ``0``, performs full AMR.


.. confval:: web_dir

   :type: string
   :default: N/A

   The ``public_html`` directory in which the summary page will be created.


.. confval:: gracedb_url

   :type: string
   :default: https://gracedb.ligo.org/api/

   URI for the GraceDB API used to interact with events.  Should include the
   ``/api/`` portion of the URI.


.. confval:: use_skymap

   :type: integer

   Uses an existing skymap file if set to ``1``, or not if set to ``0``.

   If set to ``1``, you must specify the file in :confval:`skymap_file`


.. confval:: use_event_spin

   :type: boolean

   Uses trigger :math:`\chi_z` values file if set to ``True``, or assumes zero
   spin if set to False.


.. confval:: cProfile

   :type: boolean
   :default: ``False``

   If ``True``, each ILE job will be time profiled.

.. confval:: event_parameters_in_config_file

   :type: boolean

   .. warning::

      Currently values other than ``0`` are unsupported.

   If set to ``0``, event parameters are taken from GraceDB.  If set to ``1``,
   event parameters will be taken from the `Event`_ section.


.. confval:: n_iterations_per_job

   :type: integer

   The number of iterations to perform.  ``1`` would only compute the initial
   grid, ``2`` would refine once with AMR, etc.


.. confval:: accounting_group

   :type: string

   The accounting group to use on Condor.  Options can be found
   `here <https://ldas-gridmon.ligo.caltech.edu/ldg_accounting/user>`_.


.. confval:: accounting_group_user

   :type: string
   :default: the username used to run the program

   The accounting group username to use on Condor.


.. confval:: overwrite_existing_event_dag

   :type: integer
   :default: ``0``

   If set to ``1``, overwrites an existing output directory, useful for
   testing.


.. confval:: submit_dag

   :type: integer

   Auto-submits the generated DAG if set to ``1``.  Only generates the DAG if
   set to ``0``.


.. confval:: email_address_for_job_complete_notice

   :type: string
   :default: N/A

   If set, sends an email to the specified address once all jobs and plotting
   scripts have completed.


.. confval:: intrinsic_param_to_search

   :type: list of strings
   :default: ``[mass1,mass2]``

   A list of the intrinsic parameters to search over.  Choices are:

   * ``[mass1,mass2]``
   * ``[mass2,mass2,spin1z,spin2z]``

   If set to ``[mass1,mass2]``, the intrinsic grid will be 2D and rectilinear
   in two mass parameters.  If set to ``[mass1,mass2,spin1z,spin2z]``, the
   intrinsic grid will be 4D and rectilinear in two mass and two spin
   parameters.  However, the actual mass and spin parameters will depend on
   :confval:`distance_coordinates`


.. confval:: exe_integration_extrinsic_likelihood

   :type: string
   :default: rapidpe_integrate_extrinsic_likelihood

   Executable to use for the integrate-extrinsic-likelihood / ILE step.

   Can either be a path to the executable or just the name if available in your
   system's ``$PATH``.  Names will be expanded while generating the DAG, rather
   than by Condor.


.. confval:: exe_generate_initial_grid

   :type: string
   :default: N/A

   Executable to create the intrinsic grid. If not specified
   :confval:`exe_grid_refine` is used for initial grid.

   Can either be a path to the executable or just the name if available in your
   system's ``$PATH``.  Names will be expanded while generating the DAG, rather
   than by Condor.


.. confval:: exe_grid_refine

   :type: string
   :default: rapidpe_compute_intrinsic_grid

   Executable which performs AMR.

   Can either be a path to the executable or just the name if available in your
   system's ``$PATH``.  Names will be expanded while generating the DAG, rather
   than by Condor.


.. confval:: exe_create_event_dag

   :type: string
   :default: rapidpe_create_event_dag

   Executable which sets up the dag to run the ILE integration for every
   intrinsic grid point. It is run once per iteration.

   Can either be a path to the executable or just the name if available in your
   system's ``$PATH``.  Names will be expanded while generating the DAG, rather
   than by Condor.



-----
Event
-----

There are several different modes one can specify an event with.  Each one is
listed as a different sub-section in this document, but in an actual config
file they are all part of the `Event`_ section.


^^^
gid
^^^

This mode specifies an event on GraceDB using its GraceDB ID


.. confval:: gracedb_id

   :type: string

   The event's GraceDB ID.


.. confval:: run_mode

   :type: string
   :default: online

   The mode we're running in, for the purpose of choosing channel names.
   Choices are:

   * ``online``
   * ``o3replay``
   * ``o2replay``
   * ``gracedb``

   .. note::
      Unlike the others, ``gracedb`` will look up the channel names from
      GraceDB.


.. confval:: query_shm

   :type: boolean
   :default: ``False``

   When enabled, frame files are first looked for in the high-speed, low-latency
   `Shared Memory`_ cache available on some LDAS resources under
   ``/dev/shm/kafka``.

   When disabled, or if it fails to find the frames, GWPy_ is used to look for
   the data locally with GWDataFind, and then remotely with NDS.  For more
   details, see `GWPy TimeSeries.get docs`_.


.. confval:: mdc_event_injection_file

   :type: string
   :default: N/A

   Specify path to the injection XML file if the event is part of an MDC.


.. confval:: mdc_time_offset

   :type: string
   :default: N/A

   If :confval:`mdc_event_injection_file` is specified, specify the time offset
   of the MDC from the injection file.


.. confval:: frame_data_types

   :type: dictionary
   :default: ``{"H1" : "H1_HOFT_C00", "L1" : "L1_HOFT_C00", "V1" : "V1Online"}``

   Specify the frame type of the data  for each detector.


^^^
sid
^^^

This mode specifies an event on GraceDB using its superevent ID to fetch the
corresponding preferred event.

.. confval:: superevent_id

   :type: string

   The superevent's ID.  The corresponding preferred event will be used.


.. confval:: run_mode

   :type: string
   :default: online

   The mode we're running in, for the purpose of choosing channel names.
   Choices are:

   * ``online``
   * ``o3replay``
   * ``o2replay``
   * ``gracedb``

   .. note::
      Unlike the others, ``gracedb`` will look up the channel names from
      GraceDB.


.. confval:: query_shm

   :type: boolean
   :default: ``False``

   When enabled, frame files are first looked for in the high-speed, low-latency
   `Shared Memory`_ cache available on some LDAS resources under
   ``/dev/shm/kafka``.

   When disabled, or if it fails to find the frames, GWPy_ is used to look for
   the data locally with GWDataFind, and then remotely with NDS.  For more
   details, see `GWPy TimeSeries.get docs`_.


.. confval:: mdc_event_injection_file

   :type: string
   :default: N/A

   Specify path to the injection XML file if the event is part of an MDC.


.. confval:: mdc_time_offset

   :type: string
   :default: N/A

   If :confval:`mdc_event_injection_file` is specified, specify the time offset
   of the MDC from the injection file.


.. confval:: frame_data_types

   :type: dictionary
   :default: ``{"H1" : "H1_HOFT_C00", "L1" : "L1_HOFT_C00", "V1" : "V1Online"}``

   Specify the frame type of the data  for each detector.


^^^^^^^^^^
injections
^^^^^^^^^^

This mode specifies a list of injections and the data to run RapidPE.


.. confval:: injections_file

   :type: string
   :default: /home/caitlin.rose/my_rapidPE_work/f2y2016data/subset_f2y2016inj/exact_inj/f2y2016_HLV_100shuffled_exactmasses.txt

   List of injections. The columns in this file should include:

   * ``mass1``
   * ``mass2``
   * ``spin1z``
   * ``spin2z``
   * ``latitude``
   * ``longitude``
   * ``distance``
   * ``psi0``
   * ``phi0``
   * ``inclination``
   * ``geocent_end_time``
   * ``original_index``


.. confval:: cache_file

   :type: string
   :default: /home/caitlin.rose/my_rapidPE_work/f2y2016data/create_2016_L1H1_data/2016injecteddata/injected_frames.cache

   Cache file containing path to the data.


.. confval:: psd_file

   :type: special list
   :default: ["H1=/home/caitlin.rose/my_rapidPE_work/f2y2016data/create_2016_L1H1_data/firstattempt/H1_psd_mid.xml.gz", "L1=/home/caitlin.rose/my_rapidPE_work/f2y2016data/create_2016_L1H1_data/firstattempt/L1_psd_mid.xml.gz", "V1=/home/caitlin.rose/my_rapidPE_work/f2y2016data/check_virgo_psd/V1_psd.xml.gz"]

   Path to psd files for each detector.


.. confval:: skymap_file

   :type: string
   :default: /home/vinaya.valsan/rapidPE/sinead_rapidPE/pp-plots-from-scratch/skymap/2016_fits/$INJINDEX$/bayestar.fits.gz

   Path to the Bayestar skymap file. If specified, these skymaps will be used as a prior for ILE.

.. confval:: channel_name

   :type: special list
   :default: ``["H1=INJ_GAUSSIAN","L1=INJ_GAUSSIAN","V1=INJ_GAUSSIAN"]``

   Data channel name.


.. confval:: frame_data_types

   :type: dictionary
   :default: ``{"H1" : "H1_HOFT_C00", "L1" : "L1_HOFT_C00", "V1" : "V1Online"}``

   Specify the frame type of the data  for each detector.


---------------
InitialGridOnly
---------------

This section is passed verbatim to the executable specified in
:confval:`exe_grid_refine`.

.. confval:: total-points

   :type: integer

   Requested total number of points in the initial grid, meant to be used when
   :confval:`distance-coordinates` is different than the parameters used to set
   the prior bounds.  Note that the actual number of points may be higher than
   requested.  Our approach is to guess an equal number of points along each
   dimension, apply the prior bounds, and then increase the number of points
   until ``total-points`` has been met or exceeded.


.. confval:: points-per-side

   :type: integer

   The number of grid points per dimension for intrinsic grid.
   NOTE: This will be overwritten if :confval:`total-points` is set.


.. confval:: overlap-threshold

   :type: float

   If using :confval:`overlap_bank`, this number is used as the overlap
   threshold for the initial grid.


.. confval:: mc-min

    :type: float

    Restrict
    :math:`\mathcal{M}_{\mathrm{c}} \geq \mathcal{M}_{\mathrm{c,min}}`.


.. confval:: mc-max

    :type: float

    Restrict
    :math:`\mathcal{M}_{\mathrm{c}} \leq \mathcal{M}_{\mathrm{c,max}}`.


----------------
InitialGridSetup
----------------

There are several different modes one can specify the initial grid setup with.
Each one is listed as a different sub-section in this document, but in an
actual config file they are all part of the `InitialGridSetup`_ section.


^^^^^^^^^^^^
overlap_bank
^^^^^^^^^^^^

.. confval:: overlap_bank

   :type: string

   The overlaps from here will be used to decide the initial grid region.


^^^^^^^^^^^^^^
initial_region
^^^^^^^^^^^^^^

.. confval:: initial_region

   :type: dictionary

   Specify the upper and lower boundaries of initial grid region for each
   grid parameter. If the grid is in 2D, the dict keys should include
   parameters from :confval:`distance_coordinates`.  If the grid is in 4D and
   :confval:`distance_coordinates` is ``mu1_mu2_q_spin2z``, the dict keys should
   be ``[mu1, mu2,q,spin2z]``.  If grid is in 4D, but
   :confval:`distance_coordinates` contains two parameters, the dict keys
   should include  the parameter from :confval:`distance_coordinates` and two
   spin parameters.  Allowed set of spin parameter are ``[spin1z, spin2z]`` and
   ``[chi_eff, chi_a]``.


^^^^^^^^^^^^^^^^^^
search_bias_bounds
^^^^^^^^^^^^^^^^^^

.. confval:: search_bias_bounds_spec

   :type: string
   :default: ``rapidpe_rift_pipe.config_files.search_bias_bounds``/default.json

   Specify the boundaries for the initial grid based on a fit to the search
   bias.  The default points to a fit to GstLAL search biases on MDC data.


^^^^^^^^^^
svd_bounds
^^^^^^^^^^

.. confval:: svd_bounds_file

   :type: string

   TODO


.. confval:: svd_depth_json

   :type: string

   Should be a JSON file with a list of dictionaries of the form::

      {"bounds": {"param0": [param0_min, param0_max],
                  "param1": [param1_min, param1_max]},
       "fudge_factors": {"param0": fudge_factor_0,
                         "param1": fudge_factor_1},
       "svd_depth": depth}

   For a given trigger, we find the dictionary for which it falls in the
   bounds for each parameter, and then set the initial region to enclose
   the ``svd_depth`` highest-SNR SVD bins, and add on the fudge factor
   (as a fraction of the initial range) to each parameter's limits.


.. confval:: svd_bin_params

   :type: list of strings
   :default: ``[mchirp, eta]``

   TODO


----------
GridRefine
----------

The arguments in this section are used by :confval:`exe_grid_refine`.

.. confval:: distance_coordinates

   :type: string
   :default: N/A

   The mass coordinates in which the intrinsic grid is rectilinear. Used for
   both initial grid and grid refinements. Choices are:

   * ``mchirp_q``
   * ``mchirp_eta``
   * ``mtotal_q``
   * ``tau0_tau3``
   * ``mu1_mu2_q_spin2z``


.. confval:: no-exact-match

   Loosen criteria that the input intrinsic point must be a member of the input
   template bank.


.. confval:: verbose

   Produce verbose output.


.. confval:: intrinsic-param

   :type: list of strings
   :default: ``[mass1, mass2]``

   Adapt the grid in these intrinsic parameter.


.. confval:: overlap-thresh

   :type: float between ``0.0`` and ``1.0``

   Points whose marginal likelihood are within this fraction of the maximum
   likelihood point will be considered for refinement.

   For example, ``1.0`` would consider all points, ``0.0`` would only consider
   the maximum likelihood point, and ``0.9`` would consider points with
   marginal likelihoods at least ``0.1`` times the maximum likelihood value.


---------------------
LikelihoodIntegration
---------------------

The arguments in this section are used by
:confval:`exe_integration_extrinsic_likelihood`.  Use any variable that
:confval:`exe_integration_extrinsic_likelihood` can take.  Underscores in
parameter names will be converted to hyphens, and two hyphens will precede
each argument.  If you want to use a command line switch like ``--foo``, you
need to set the configuration variable ``foo`` to the empty string, i.e.,
``foo=``.


For example:

* ``x=y`` will be converted to ``--x y``
* ``arg_a=foo`` will be converted to ``--arg-a foo``
* ``has_no_arg=`` will be converted to ``--has-no-arg``


------
Pastro
------

The arguments in this section are used for the :math:`p_{\mathrm{astro}}` calculation.

.. confval:: category_rates
    :type: dict
    
    Specify categorywise rates from O3. The keys for the dictionary are

    * ``R_bns``
    * ``R_nsbh``
    * ``R_bbh``

.. confval:: prior_boundary
    :type: dict

    Specify the prior boundary for pastro calculation. The keys for the dictionary are

    * ``m_max_bank``
    * ``m_min_bank``
    * ``m_max_ns``
    * ``q_min_bank``
    * ``q_max_bank``

.. confval:: rankstat_pdf_file
    :type: str

    Path to the GstLAL rankingstatpdf file. 

.. confval:: far_threshold
    :type: float

    FAR threshold to be used for :math:`p_{\mathrm{astro}}` calculation. 



.. _GWPy: https://gwpy.github.io/

.. _GWPy TimeSeries.get docs: https://gwpy.github.io/docs/latest/timeseries/datafind/#timeseries-get

.. _Shared Memory: https://en.wikipedia.org/wiki/Shared_memory
