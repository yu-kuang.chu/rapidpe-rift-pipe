# RapidPE/RIFT Pipeline

Generates pipelines for running RapidPE and RIFT on HTCondor.


# Credit

Copyright (c) 2022 Caitlin Rose, Vinaya Valsan, Daniel Wysocki, Sinead Walsh, Patrick Brady

This software may be freely distributed under the terms of the GNU General Public License v2.  See [`COPYING`](./COPYING) for details.
